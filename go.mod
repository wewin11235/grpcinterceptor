module grpcinterceptor

go 1.14

require (
	google.golang.org/grpc v1.37.1
	google.golang.org/protobuf v1.26.0
)
