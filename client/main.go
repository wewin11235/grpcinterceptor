package main

import (
	"context"
	"google.golang.org/grpc"
	interceptor "grpcinterceptor/proto"
	"log"
)

func main() {
	conn, err := grpc.Dial("localhost:11088", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("grpc dial err: %v", err)
	}
	defer conn.Close()

	sayHiClient := interceptor.NewSayHiServiceClient(conn)

	request :=  &interceptor.SayHiRequest{Name: "wei.wei"}
	sayHiReply, err := sayHiClient.SayHi(context.Background(), request)
	if err != nil {
		log.Fatalf("client rpc sayHi err: %v", err)
	}

	log.Println(sayHiReply.Name)
}
