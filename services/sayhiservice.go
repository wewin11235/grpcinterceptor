package services

import (
	"context"
	interceptor "grpcinterceptor/proto"
)

type SayHiService struct {
	interceptor.UnimplementedSayHiServiceServer
}

func (s *SayHiService) SayHi(ctx context.Context, in *interceptor.SayHiRequest) (*interceptor.SayHiResponse, error) {
	return &interceptor.SayHiResponse{Name: in.Name}, nil
}